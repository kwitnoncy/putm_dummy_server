import socket
import json
import time
from cv2 import waitKey

running = True
counter = 0
json_data = {
    "id": 0,
    "timestamp": 0,
    "values": []
}
json_conf = {
    "sensors": [
        {"id": 10, "name": "front left wheel speed", "up": 120, "down": 0, "unit": "km/h"},
        {"id": 11, "name": "front right wheel speed", "up": 120, "down": 0, "unit": "km/h"},
        {"id": 12, "name": "rear left wheel speed", "up": 120, "down": 0, "unit": "km/h"},
        {"id": 13, "name": "rear right wheel speed", "up": 120, "down": 0, "unit": "km/h"}
    ]
}

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind(('10.160.43.135', 8000))
socket.listen(5)

client_sock, client_addr = socket.accept()
print("connection from: ", client_addr)

data = json.dumps(json_conf)
client_sock.send(data.encode(encoding="utf-8"))

while running:
    if counter % 4 == 0:
        json_data["id"] = 10
        json_data["timestamp"] = counter
        json_data["values"] = 100 + counter
        data = json.dumps(json_data)
        client_sock.send(data.encode(encoding="utf-8"))
        counter = counter+1
    elif counter % 4 == 1:
        json_data["id"] = 11
        json_data["timestamp"] = counter
        json_data["values"] = 200 + counter
        data = json.dumps(json_data)
        client_sock.send(data.encode(encoding="utf-8"))
        counter = counter+1
    elif counter % 4 == 2:
        json_data["id"] = 12
        json_data["timestamp"] = counter
        json_data["values"] = 300 + counter
        data = json.dumps(json_data)
        client_sock.send(data.encode(encoding="utf-8"))
        counter = counter + 1
    elif counter % 4 == 3:
        json_data["id"] = 13
        json_data["counter"] = counter
        json_data["values"] = 400 + counter
        data = json.dumps(json_data)
        client_sock.send(data.encode(encoding="utf-8"))
        counter = counter+1

    if waitKey(5) == ' ':
        running = False

    time.sleep(1)


socket.close()
